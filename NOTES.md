Notes
=====

Switch Between TTYs
-------------------

- Alt-F*n* — switch to tty*n*
- Alt-Left — switch to previous tty
- Alt-Right — switch to next tty

**TODO**: Figure out how to remap Alt-Left/-Right as they're
interfering with e.g. movement in Emacs.

It's also possible to switch between TTYs using commands:

    chvt 1    # swith to tty1
    tty       # verify you're in tty1
    fgconsole # print number of active ttys


Make Caps Lock another Ctrl Key
-------------------------------

Edit `/etc/default/keyboard`.

Change

    XKBOPTIONS=""

to

    XKBOPTIONS="ctrl:nocaps"

Save.

Run

    sudo dpkg-reconfigure -phigh console-setup

See https://www.emacswiki.org/emacs/MovingTheCtrlKey
