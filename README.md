Scripts and Notes for My Framebuffer-only Linux Laptop
======================================================

I have installed Debian 11 without X (i.e. framebuffer-only) on a
Lenovo X100e. This repositories contains some [notes](NOTES.md) on the
installation and configuration along with [scripts](scripts/) for
working on the laptop.
