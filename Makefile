.PHONY: show-help install-tools lint install clean

OUTPUT_DIR  = .output

SCRIPTS    := $(shell find scripts/ -type f -not -name '*.conf')

# Default target to display help text ##################################

show-help:
	@echo "The following targets are available:"
	@echo
	@echo "\t- show-help (display this help text)"
	@echo "\t- install-tools (install required tools)"
	@echo "\t- lint (check scripts)"
	@echo "\t- install (install into $$HOME)"
	@echo "\t- clean (delete generated files)"
	@echo

# Target to create OUTPUT_DIR user by other targets ####################

 $(OUTPUT_DIR)/:
	mkdir -p $@


# Targets for installing required tools ################################

$(OUTPUT_DIR)/apt-get-install: $(OUTPUT_DIR)/
	sudo apt-get install shellcheck
	touch $@

install-tools: $(OUTPUT_DIR)/apt-get-install



# Targets for linting scripts ##########################################

$(OUTPUT_DIR)/run-shellcheck: $(SCRIPTS) $(OUTPUT_DIR)/
	shellcheck $<
	touch $@

lint: $(OUTPUT_DIR)/run-shellcheck


# Target to install into $HOME #########################################

install:
	ln -s "$$PWD/scripts/"* "$$HOME/bin/"

# Target to delete generated files #####################################

clean:
	rm -fR $(OUTPUT_DIR)
